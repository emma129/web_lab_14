package ictgradschool.web.lab14.ex3New;

/**
 * Created by yzhb363 on 10/05/2017.
 */
public class Genre {
    private String genre;

    public Genre(String genre){
        this.genre = genre;
    }

    public void setGenre(String genre){
        this.genre = genre;
    }

    public String getGenre(){
       return genre;
    }
}
