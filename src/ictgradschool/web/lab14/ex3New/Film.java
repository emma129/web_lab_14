package ictgradschool.web.lab14.ex3New;

/**
 * Created by yzhb363 on 9/05/2017.
 */
public class Film {
   private String filmName,genreName;

    public Film(String filmName,String genreName){
        this.filmName = filmName;
        this.genreName = genreName;
    }

    public void setFilmName (String filmName){
        this.filmName =filmName;
    }

    public String getFilmName(){
        return filmName;
    }

    public void setGenreName (String filmName) {
        this.genreName = filmName  ;
    }
    public String getGenreName() {
        return genreName;
    }

}
