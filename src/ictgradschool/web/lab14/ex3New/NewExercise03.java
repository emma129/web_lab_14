package ictgradschool.web.lab14.ex3New;

import ictgradschool.Keyboard;
import ictgradschool.web.lab14.ex3.Actor;
import ictgradschool.web.lab14.ex3.Film;
import ictgradschool.web.lab14.ex3.FilmInfoDAO;
import ictgradschool.web.lab14.ex3.Genre;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yzhb363 on 10/05/2017.
 */
public class NewExercise03 {

    public void start() {

        System.out.println("Welcome to the Film database");


        while (true){

            WelcomeWords();
            String userInput = Keyboard.readInput();
            switch (userInput) {
                case "1":
                    getFilmbyActor();
                    break;
                case "2":
                    getActor();
                    break;
                case "3":
                    getFilmbyGenre();
                    break;
                case "4":
                    System.exit(0);
                    break;
            }


        }

    }

    public static void WelcomeWords() {

        System.out.println();
        System.out.println("Please select an option from the following:\n" +
                "1. Information by Actor\n" +
                "2. Information by Movie\n" +
                "3. Information by Genre\n" +
                "4. Exit");

    }



    public void getFilmbyActor() {
        FilmDAO film = new FilmDAO();
        System.out.println("Please enter the name of the actor you wish to\n" +
                "get information about, or press enter to return\n" +
                "to the previous menu");
        String userInput = Keyboard.readInput();
        film.getFilmDAO(userInput);

    }

    public void getActor() {
        ActorDAO actor = new ActorDAO();

        System.out.println("Please enter the name of the actor you wish to\n" +
                "get information about, or press enter to return\n" +
                "to the previous menu");
        String userInput = Keyboard.readInput();

        actor.getFilmbyActor(userInput);

    }

    public void getFilmbyGenre() {
        GenreDAO genreName = new GenreDAO();
        System.out.println("Please enter the name of the genre you wish to\n" +
                "get information about, or press enter to return\n" +
                "to the previous menu");
        String genre = Keyboard.readInput();
        genreName.searchByGenre(genre);
    }


    public static void main(String[] args) {

        new NewExercise03().start();
    }
}
