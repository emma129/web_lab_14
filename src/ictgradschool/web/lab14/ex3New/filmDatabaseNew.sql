DROP TABLE IF EXISTS ex03_involve;
DROP TABLE IF EXISTS ex03_films;
DROP TABLE IF EXISTS ex03_genre;
DROP TABLE IF EXISTS ex03_actors;


CREATE TABLE ex03_actors (
  actorid int NOT NULL,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (actorid)
);

CREATE TABLE ex03_genre (
  genre VARCHAR(50) NOT NULL,
  PRIMARY KEY (genre)
);

CREATE TABLE ex03_films (
  filmid int NOT NULL,
  name VARCHAR(50) NOT NULL,
  genre VARCHAR(50) NOT NULL,
  PRIMARY KEY (filmid),
  FOREIGN KEY (genre) REFERENCES ex03_genre(genre)
);

CREATE TABLE ex03_involve (
  filmid int NOT NULL,
  actorid int NOT NULL,
  role VARCHAR(50) NOT NULL,
  PRIMARY KEY (filmid,actorid),
  FOREIGN KEY (filmid) REFERENCES ex03_films(filmid),
  FOREIGN KEY (actorid) REFERENCES ex03_actors(actorid)
);

INSERT INTO ex03_actors VALUES
  (1,'Blueberry'),
  (2,'Thomas Liu'),
  (3,'Strawberry Wong'),
  (4,'Apple Li');

INSERT INTO ex03_genre VALUES
  ('Romantic'),
  ('Horro');

INSERT INTO ex03_films VALUES
  (1,'Harry Potter','Romantic'),
  (2,'Diary','Romantic'),
  (3,'Love Actually','Horro'),
  (4,'Smurf','Romantic'),
  (5,'Fast8','Horro');

INSERT INTO ex03_involve VALUES
  (1,1,'Lead'),
  (2,1,'Lead'),
  (3,2,'Lead'),
  (3,3,'Director'),
  (3,4,'Director');

