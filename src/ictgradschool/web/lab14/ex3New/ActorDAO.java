package ictgradschool.web.lab14.ex3New;

import ictgradschool.Keyboard;
import ictgradschool.web.lab14.Config;
import ictgradschool.web.lab14.ex3.*;

import java.sql.*;

/**
 * Created by yzhb363 on 10/05/2017.
 */
public class ActorDAO {

    public void getFilmbyActor(String userInput){
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/yzhb363", Config.getProperties())) {
            // System.out.println("Connection successful");


            try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM ex03_actors,ex03_films,ex03_involve WHERE ex03_films.name LIKE ? AND ex03_actors.actorid=ex03_involve.actorid AND ex03_films.filmid=ex03_involve.filmid")) {


                stmt.setString(1,"%" + userInput + "%" );

                try (ResultSet resultNew = stmt.executeQuery()) {
                    if (!resultNew.isBeforeFirst()){
                        System.out.println("\nSorry, we couldn't find any films by that name.\n");
                        return;
                    }
                    // Loop through each row...
                    while (resultNew.next()) {
                        String actorInfo = resultNew.getString(resultNew.findColumn("ex03_actors.name")) + " ("+ resultNew.getString(resultNew.findColumn("ex03_involve.role"))+ ")";
                        System.out.println(actorInfo);
                    }
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }




    }


}
