package ictgradschool.web.lab14.ex3New;

import ictgradschool.Keyboard;
import ictgradschool.web.lab14.Config;

import java.sql.*;

/**
 * Created by yzhb363 on 10/05/2017.
 */
public class GenreDAO {
    public void searchByGenre(String genre) {

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }


        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/yzhb363", Config.getProperties())) {

            try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM ex03_films WHERE genre LIKE ?")) {
                stmt.setString(1, "%" + genre + "%");
                try (ResultSet newResult = stmt.executeQuery()) {
                    if (!newResult.isBeforeFirst()) { //return true if the cursor is before the first row.
                        System.out.println("Sorry, we couldn't find any films by that genre\n");
                        return;
                    }
                    else {
                        System.out.println("The " + genre + " genre includes the following films:\n");
                        while (newResult.next()) {
                            String filmName = newResult.getString(newResult.findColumn("name"));
                            System.out.println(filmName);
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
//
//    public void getFilmDAO(){
//        try {
//            Class.forName("com.mysql.jdbc.Driver");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        // Set the database name to your database
//        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/yzhb363", Config.getProperties())) {
//            // System.out.println("Connection successful");
//            System.out.println("Please enter the name of the actor you wish to\n" +
//                    "get information about, or press enter to return\n" +
//                    "to the previous menu");
//            String userInput = Keyboard.readInput();
//
//            try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM lab14_actors,lab14_films,lab14_involve WHERE lab14_actors.actorid=lab14_involve.actorid AND lab14_films.filmid=lab14_involve.filmid AND lab14_actors.name LIKE ?")) {
//
//                if (userInput.isEmpty()){
//                    return;
//                }
//                else
//                    stmt.setString(1,"%" + userInput + "%" );
//
//                try (ResultSet r = stmt.executeQuery()) {
//
//                    if (!r.isBeforeFirst()){
//                        System.out.println("\nSorry, we couldn't find any films by that name.\n");
//                        return;
//                    }
//                    // Loop through each row...
//                    System.out.println("\n" + r.findColumn("lab14_actors.name") + " is listed as being involved in the following films: \n");
//                    while (r.next()) {
//
//                        String actorInfo = r.getString(r.findColumn("ex03_films.name")) + " ("+ r.getString(r.findColumn("ex03_involve.role"))+ ")";
//
//                        System.out.println(actorInfo);
//
//                    }
//                }
//
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//    }

