package ictgradschool.web.lab14.ex3New;

import ictgradschool.Keyboard;
import ictgradschool.web.lab14.Config;


import java.sql.*;

/**
 * Created by yzhb363 on 10/05/2017.
 */
public class FilmDAO {

    public void getFilmDAO(String userInput) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/yzhb363", Config.getProperties())) {
            // System.out.println("Connection successful");


            try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM ex03_actors,ex03_films,ex03_involve WHERE ex03_actors.actorid=ex03_involve.actorid AND ex03_films.filmid=ex03_involve.filmid AND ex03_actors.name LIKE ?")) {

                stmt.setString(1, "%" + userInput + "%");

                try (ResultSet r = stmt.executeQuery()) {

                    if (!r.isBeforeFirst()) {
                        System.out.println("\nSorry, we couldn't find any films by that name.\n");
                        return;
                    }
                    // Loop through each row...
                    System.out.println("\n" + r.findColumn("ex03_actors.name") + " is listed as being involved in the following films: \n");
                    while (r.next()) {

                        String actorInfo = r.getString(r.findColumn("ex03_films.name")) + " (" + r.getString(r.findColumn("ex03_involve.role")) + ")";

                        System.out.println(actorInfo);

                    }
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
