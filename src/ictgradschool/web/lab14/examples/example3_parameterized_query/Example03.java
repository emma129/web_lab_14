package ictgradschool.web.lab14.examples.example3_parameterized_query;

import ictgradschool.web.lab14.Config;

import java.sql.*;

public class Example03 {
    public static void main(String[] args) {

        // Load JDBC driver
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/" + Config.DB_NAME, Config.getProperties())) {
            System.out.println("Connection successful");

            // Make a query
            try (PreparedStatement stmt = conn.prepareStatement("SELECT * from unidb_courses WHERE dept = ? and num < ?;")) {

                // Set the parameters of the query. Note that the indices are 1-based, not 0-based.
                stmt.setString(1, "comp");
                stmt.setInt(2, 300);

                try (ResultSet r = stmt.executeQuery()) {

                    // Loop through each row...
                    while (r.next()) {

                        // Print out the contents of that row. You can get an individual column value by using getString, getInt, etc...
                        // You can supply either the column index (which is 1-based, not 0-based as usual), or the name of the column.

                        System.out.println("Department: " + r.getString(1) +
                                ", Number: " + r.getInt("num") +
                                ", Description: " + r.getString(3));

                    }

                }
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
