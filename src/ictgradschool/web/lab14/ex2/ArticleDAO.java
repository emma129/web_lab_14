package ictgradschool.web.lab14.ex2;

import ictgradschool.Keyboard;
import ictgradschool.web.lab14.Config;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static javafx.scene.input.KeyCode.R;

/**
 * Created by yzhb363 on 9/05/2017.
 */
public class ArticleDAO {

    public List<Article> allArticles(String userInput) {

        List<Article> articles = new ArrayList<>();

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/yzhb363", Config.getProperties())) {
            //System.out.println("Connection successful");


            try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM simpledao_articles WHERE title LIKE ?;")) {
                stmt.setString(1, "%" + userInput + "%");
                try (ResultSet r = stmt.executeQuery()) {
                    while (r.next()) {
                        Article a = new Article();

                        a.setTitle(r.getString(2));
                        a.setText(r.getString(3));
                        articles.add(a);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return articles;
    }


}
