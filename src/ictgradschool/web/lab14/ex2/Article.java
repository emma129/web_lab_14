package ictgradschool.web.lab14.ex2;

/**
 * Created by yzhb363 on 9/05/2017.
 */
public class Article {
    private String title;
    private String text;



//   public Article(String title, String text){
//       this.text =text;
//       this.title =title;
//
//   }

    public void setTitle(String s) {
        this.title = s;
    }

    public void setText(String s) {
        this.text = s;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }
    public String toString(){
        String article = title + "\n" + text;
        return article;
    }


}
