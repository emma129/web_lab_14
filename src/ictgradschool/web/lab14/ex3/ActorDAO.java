package ictgradschool.web.lab14.ex3;
import ictgradschool.Keyboard;
import ictgradschool.web.lab14.Config;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by yzhb363 on 10/05/2017.
 */
public class ActorDAO {

    public List<Actor> allActors() {
        List<Actor> articles = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/yzhb363", Config.getProperties())) {
           // System.out.println("Connection successful");

            try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM ex03_actortable")) {

                try (ResultSet r = stmt.executeQuery()) {

                    // Loop through each row...
                    while (r.next()) {

                        String actorName = r.getString(r.findColumn("actorName"));
                        articles.add( new Actor(actorName));
                    }
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return articles;
    }
}
