package ictgradschool.web.lab14.ex3;

import java.sql.*;
import ictgradschool.web.lab14.Config;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yzhb363 on 10/05/2017.
 */
public class GenreDAO {
    public List<Genre> allGenre() {
        List<Genre> allGenre = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/yzhb363", Config.getProperties())) {
            System.out.println("Connection successful");

            try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM ex03_genreList")) {

                try (ResultSet r = stmt.executeQuery()) {

                    // Loop through each row...
                    while (r.next()) {

                        String genreName = r.getString(r.findColumn("genreName"));
                        allGenre.add( new Genre(genreName));
                    }
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return allGenre;
    }

}
