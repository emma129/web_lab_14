package ictgradschool.web.lab14.ex3;

/**
 * Created by yzhb363 on 10/05/2017.
 */
public class Actor {
    private String actorName;

    public Actor(String actorName){
        this.actorName = actorName;
    }
    public void setActorName(String s){
        s = actorName;
    }
    public String getActorName(){
        return actorName;
    }

}
