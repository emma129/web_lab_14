package ictgradschool.web.lab14.ex3;

/**
 * Created by yzhb363 on 10/05/2017.
 */
public class FilmAndGenre {

    String genreName;
    String filmName;

    public FilmAndGenre(String filmName,String genreName) {
        this.genreName = genreName;
        this.filmName = filmName;
    }

    public String getGenreName() {
        return genreName;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setGenreName(String producer) {
        this.genreName = genreName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

}
