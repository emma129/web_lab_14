package ictgradschool.web.lab14.ex3;

import ictgradschool.web.lab14.Config;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yzhb363 on 9/05/2017.
 */
public class FilmDAO {

    public List<Film> allFilms() {
        List<Film> articles = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/yzhb363", Config.getProperties())) {
            System.out.println("Connection successful");

            try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM ex03_filmList")) {

                try (ResultSet r = stmt.executeQuery()) {

                    // Loop through each row...
                    while (r.next()) {

                        String filmName = r.getString(r.findColumn("fimlName"));
                        articles.add( new Film(filmName));
                    }
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return articles;
    }

}
