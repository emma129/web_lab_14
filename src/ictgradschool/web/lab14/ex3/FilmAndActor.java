package ictgradschool.web.lab14.ex3;

/**
 * Created by yzhb363 on 10/05/2017.
 */
public class FilmAndActor {
    String actorName;
    String filmName;

    public FilmAndActor(String filmName,String actorName) {
        this.actorName = actorName;
        this.filmName = filmName;
    }

    public String getActorName() {
        return actorName;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setActorName(String producer) {
        this.actorName = actorName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

}
