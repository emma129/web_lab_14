package ictgradschool.web.lab14.ex3;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import ictgradschool.Keyboard;
import ictgradschool.web.lab14.Config;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yzhb363 on 10/05/2017.
 */
public class Exercise03 {
    public FilmInfoDAO infoCollection;
    public List<Film> allFimls;
    public List<Actor> allActors;
    public List<Genre> allGenre;


    public void start() {

        infoCollection = new FilmInfoDAO();
        allFimls = infoCollection.allInfo().getFimlList();
        allActors = infoCollection.allInfo().getActorList();
        allGenre = infoCollection.allInfo().getGenreList();

        WelcomeWords();

        String userinput = Keyboard.readInput();
        switch (userinput) {
            case "1":
                actorInfo();
                break;
            case "2":
                movieInfo();
                break;
            case "3":
                genereInfo();
                break;
            case "4":
                quitSystem();
                break;
            case "":
                WelcomeWords();
                break;

        }


    }

    public void WelcomeWords() {
        System.out.println("Welcome to the Film database");
        System.out.println();
        System.out.println("Please select an option from the following:\n" +
                "1. Information by Actor\n" +
                "2. Information by Movie\n" +
                "3. Information by Genre\n" +
                "4. Exit");

    }

    public void actorInfo() {
        System.out.println("Please enter the name of the actor you wish to\n" +
                "get information about, or press enter to return\n" +
                "to the previous menu");

        String actorname = Keyboard.readInput();

        ArrayList<String> movies = new ArrayList<>();

        if (actorname.isEmpty()) {
            WelcomeWords();
        }
        for (Actor actors : allActors) {

            if (actorname.equals(actors.getActorName())) {



            }
            System.out.println("is listed as being involved in the\n" +
                    "following films:\n" + movies);

        }


    }

    public void movieInfo() {

    }

    public void genereInfo() {

    }

    public void quitSystem() {

        System.exit(0);

    }


    public static void main(String[] args) {

        new Exercise03().start();
    }
}
