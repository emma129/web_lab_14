DROP TABLE IF EXISTS ex03_filmList;
DROP TABLE IF EXISTS ex03_actortable;
DROP TABLE IF EXISTS ex03_genreList;

CREATE TABLE ex03_filmList (
  fimlid   INT         NOT NULL,
  fimlName VARCHAR(20) NOT NULL,
  gen_id   INT         NOT NULL,
  PRIMARY KEY (fimlid)
);
CREATE TABLE ex03_actortable (
  actorid   INT         NOT NULL,
  actorName VARCHAR(50) NOT NULL,
  fimId     INT         NOT NULL,
  PRIMARY KEY (actorid)
);

CREATE TABLE ex03_genreList (
  genreid   INT         NOT NULL,
  genreName VARCHAR(50) NOT NULL,
  PRIMARY KEY (genreid)
);

INSERT INTO ex03_filmList VALUES
  (1562, 'BJ Diary', 01),
  (1897,'Smurf',01),
  (1795,'Love Acutally',02),
  (1754,'Harry Poter',03);

INSERT INTO ex03_actortable VALUES
  (12,'EmmaRoberts',1562),
  (15,'John',1897),
  (16,'Lily',1897),
  (14,'Cameron',1795),
  (13,'Thomas',1754);

INSERT INTO ex03_genreList VALUES
  (01,'Romantic'),
  (02,'Horro'),
  (03,'Comedy');




