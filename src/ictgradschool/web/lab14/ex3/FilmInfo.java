package ictgradschool.web.lab14.ex3;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yzhb363 on 10/05/2017.
 */
public class FilmInfo {

    private List<Actor> actorList = new ArrayList<>();
    private List<Genre> genreList = new ArrayList<>();
    private List<Film> fimlList = new ArrayList<>();

    public FilmInfo( List<Film> fimlList,List<Actor> actorList, List<Genre> genreList){
        this.fimlList = fimlList;
        this.actorList = actorList;
        this.genreList = genreList;
    }

    public void setFimlList(List<Film> fimlList){
        this.fimlList = fimlList;
    }
    public void setActorList(List<Actor> actorList){
        this.actorList = actorList;
    }
    public void setGenreList(List<Genre> genreList){
        this.genreList = genreList;
    }

    public List<Film> getFimlList(){
        return fimlList;
    }


    public List<Actor> getActorList(){
        return actorList;
    }
    public List<Genre> getGenreList(){
        return genreList;
    }





}
