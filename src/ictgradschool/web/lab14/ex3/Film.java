package ictgradschool.web.lab14.ex3;

/**
 * Created by yzhb363 on 9/05/2017.
 */
public class Film {
   private String filmName;

    public Film (String filmName){
        this.filmName = filmName;
    }

    public void setFilmName (String s){
        s = this.filmName;
    }

    public String getFilmName(){
        return filmName;
    }

}
