package ictgradschool.web.lab14.ex3;

import ictgradschool.web.lab14.Config;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yzhb363 on 10/05/2017.
 */
public class FilmInfoDAO {
    public FilmInfo allInfo () {
        FilmInfo allInfoFiml = null;

        List<Film> newFilm = new ArrayList<>();
        List<Actor> newActor = new ArrayList<>();
        List<Genre> newGenre = new ArrayList<>();

        final String[] databaseNames = {"SELECT * FROM ex03_filmList", "SELECT * FROM ex03_actortable", "SELECT * FROM ex03_filmList", "SELECT * FROM ex03_genreList"};
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/yzhb363", Config.getProperties())) {
            System.out.println("Connection successful");
            for (String allDatas : databaseNames) {
                try (PreparedStatement stmt = conn.prepareStatement(allDatas)) {
                    try (ResultSet r = stmt.executeQuery()) {
                        while (r.next()) {
                            switch (allDatas){
                                case "SELECT * FROM ex03_filmlist":
                                    String filmName = r.getString(r.findColumn("filmName"));
                                    newFilm.add(new Film(filmName));
                                    break;
                                case "SELECT * FROM ex03_actortable":
                                    String actorName = r.getString(r.findColumn("actorName"));
                                    newActor.add(new Actor(actorName));
                                    break;
                                case "SELECT * FROM ex03_genreList":
                                    String genreName = r.getString(r.findColumn("genreName"));
                                    newGenre.add(new Genre(genreName));

                            }

                        }
                    }

                    allInfoFiml = new FilmInfo(newFilm,newActor,newGenre);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allInfoFiml;
    }
}


//
//                         // Loop through each row...
//                         while (r.next()) {
//
//                             String filmName = r.getString(r.findColumn("filmName"));
//                             //String actorName = l.getString(l.findColumn("actorName"));
//                             //filmandActors.add(new FilmandActor(filmName, actorName));
//                         }
//
//                     }
//
//
//                 }
//                 try (Connection onn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/yzhb363", Config.getProperties())) {
//                     try (PreparedStatement st = onn.prepareStatement("SELECT * FROM ex03_actortable")) {
//                         try (ResultSet l = st.executeQuery()) {
//                             while (l.next()) {
//                                 String actorName = l.getString(l.findColumn("actorName"));
//                             }
//                         }
//                     }
//                 }

//            public List<FilmandGenre> filmandGenres () {
//                List<FilmandGenre> filmandGenres = new ArrayList<>();
//                try {
//                    Class.forName("com.mysql.jdbc.Driver");
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//                // Set the database name to your database
//                try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/yzhb363", Config.getProperties())) {
//                    //System.out.println("Connection successful");
//
//                    try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM ex03_filmList")) {
//                        try (PreparedStatement st = conn.prepareStatement("SELECT * FROM ex03_genreList")) {
//                            try (ResultSet r = stmt.executeQuery()) {
//                                try (ResultSet l = st.executeQuery()) {
//
//                                    // Loop through each row...
//                                    while (r.next()) {
//
//                                        String filmName = r.getString(r.findColumn("filmName"));
//                                        String genreName = l.getString(l.findColumn("genreName"));
//                                        filmandGenres.add(new FilmandGenre(filmName, genreName));
//                                    }
//                                }
//                            }
//
//                        }
//                    }
//
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
//
//
//                return filmandGenres;
//            }


