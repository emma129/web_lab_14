package ictgradschool.web.lab14.ex1;

import com.sun.org.apache.bcel.internal.generic.Select;
import ictgradschool.Keyboard;
import ictgradschool.web.lab14.Config;

import java.sql.*;
import java.util.Properties;

public class Exercise01 {

    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/yzhb363", Config.getProperties())) {
            System.out.println("Connection successful");

            try (PreparedStatement stmt = conn.prepareStatement("SELECT body from simpledao_articles WHERE title LIKE ? ")) {

                while (true) {
                    System.out.println("Please enter a title");
                    String userInput = Keyboard.readInput();
                    // Set the parameters of the query. Note that the indices are 1-based, not 0-based.
                    stmt.setString(1, "%"+ userInput +"%");
                    try (ResultSet r = stmt.executeQuery()) {

                        // Loop through each row...
                        while (r.next()) {

                            // Print out the contents of that row. You can get an individual column value by using getString, getInt, etc...
                            // You can supply either the column index (which is 1-based, not 0-based as usual), or the name of the column.
                            String text = r.getString(1);
                            System.out.println("Text of this article: " + "\n" + text);
                            System.exit(0);
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
